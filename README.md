# RaspberryPi
RaspberryPi material

本仓库仅作为树莓派入门学习使用

![image](https://github.com/MakerLab308/RaspberryPi/blob/master/RPi%E4%BB%8B%E7%BB%8D/Rpi_4B.png)
<div align=center>图——树莓派4B</div>

# 树莓派入门教程
以下教程参考树莓派官网<br>建议通过[树莓派官网](https://www.raspberrypi.org/)深入学习

+ ### [Setup](https://www.raspberrypi.org/documentation/setup/)
+ ### [Installation](https://www.raspberrypi.org/documentation/installation/)
+ ### [Usage](https://www.raspberrypi.org/documentation/usage/)
+ ### [Configuration](https://www.raspberrypi.org/documentation/configuration/)
+ ### [Remote Access](https://www.raspberrypi.org/documentation/remote-access/)
+ ### [Raspbian](https://www.raspberrypi.org/documentation/raspbian/)
+ ### [Hardware](https://www.raspberrypi.org/documentation/hardware/)




# 如何使用本仓库？
本仓库包含[树莓派简介](https://github.com/MakerLab308/RaspberryPi/tree/master/RPi%E4%BB%8B%E7%BB%8D)、
[相关软件安装包](https://github.com/MakerLab308/RaspberryPi/tree/master/%E8%BD%AF%E4%BB%B6) 及
[入门项目](https://github.com/MakerLab308/RaspberryPi/tree/master/%E9%A1%B9%E7%9B%AE).

## **树莓派介绍**
包括树莓派[引脚图](https://github.com/MakerLab308/RaspberryPi/blob/master/RPi%E4%BB%8B%E7%BB%8D/RPi_pin.png)、
[版本对比](https://github.com/MakerLab308/RaspberryPi/blob/master/RPi%E4%BB%8B%E7%BB%8D/raspberrypi-version-compare.png)、
[配置文件](https://github.com/MakerLab308/RaspberryPi/blob/master/RPi%E4%BB%8B%E7%BB%8D/wpa_supplicant.conf)、
[入门资料](https://github.com/MakerLab308/RaspberryPi/blob/master/RPi%E4%BB%8B%E7%BB%8D/RPi.xmind)等资源


## **相关软件**
储存了树莓派使用过程中，常用的几款软件<br>超链接指向软件官网,您也可以从本仓库中下载安装包
- ### 系统烧录软件：
  - #### [Etcher](https://www.balena.io/etcher/)
  - #### [SDFormat](https://www.sdcard.org/downloads/formatter/)
  - #### [Win32Disk](https://sourceforge.net/projects/win32diskimager/)

- ### 远程登陆软件：
  - #### [Putty](https://www.putty.org/)
  - #### [VNC Viewer](https://www.realvnc.com/en/connect/download/viewer/)
  - #### [FinalShell](http://www.hostbuf.com/t/988.html)
  - #### [Advanced IP Scanner](http://www.advanced-ip-scanner.com/cn/)

- ### 控制软件：
  - #### [BlueDot](https://play.google.com/store/apps/details?id=com.stuffaboutcode.bluedot&hl=en)
  - #### [BonjourPS](https://support.apple.com/kb/dl999?locale=zh_CN)


## **入门项目**
- #### [人脸识别](https://github.com/MakerLab308/RaspberryPi/tree/master/%E9%A1%B9%E7%9B%AE/Face%20recognition)
- #### [GPIO音乐盒](https://github.com/MakerLab308/RaspberryPi/blob/master/%E9%A1%B9%E7%9B%AE/GPIO%20music%20box%20_%20Raspberry%20Pi%20Projects.pdf)
- #### [更多项目](https://projects.raspberrypi.org/en/)
树莓派官网提供了各类入门项目,推荐进入官网查看.

